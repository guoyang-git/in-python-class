#!/usr/bin/python
# encoding: utf-8

import pandas as pd
import matplotlib.pyplot as plt

# 标题词根拆解
# 示例标题：儿童汉服女童中国风12岁夏季薄款超仙春秋齐胸襦长袖裙唐装复古装
# 标题拆解为词根：儿童,汉服,女童,中国风,12,岁,夏季,薄款,超仙,春秋,齐胸,襦,长袖,裙,唐装,复古装
word = ['儿童','汉服','女童','中国风','12','岁','夏季','薄款','超仙','春秋','齐胸','襦','长袖','裙','唐装','复古装']
# 读取一个星期商品手淘关键词详情
stss = pd.read_excel('无线商品三级流量来源详情.xlsx')
# 读取我们需要的数据
data = stss[['来源名称','访客数','收藏人数','加购人数','支付买家数']]
# 创建一个空的表格
wordData = pd.DataFrame(columns = ['词根','访客数','收藏人数','加购人数','支付买家数'])
#
for str in word:
    data2 = data[data.来源名称.str.contains(str)]
    data3 = data2[['访客数','收藏人数','加购人数','支付买家数']]
    data3['词根'] = str
    wordData = pd.concat([wordData,data3],ignore_index=True)
wordData2 = wordData.groupby('词根').sum()
# 添加转化率
wordData2['转化率'] = wordData2['支付买家数']/wordData2['访客数']
# 添加加购率
wordData2['加购率'] = wordData2['加购人数']/wordData2['访客数']
# 添加收藏率
wordData2['收藏率'] = wordData2['收藏人数']/wordData2['访客数']

print(wordData2)
# 设置参数，以确保图像正确显示
#用来正常显示中文标签
plt.rcParams['font.sans-serif']='simhei'
# 用来正常显示负号
plt.rcParams['axes.unicode_minus']=False
# 将词根设置为x轴，访客数、转化率、加购率、收藏率设置为y轴
x=wordData2.index.values.tolist()
y1=wordData2['访客数'].values.tolist()
y2=wordData2['转化率'].values.tolist()
y3=wordData2['加购率'].values.tolist()
y4=wordData2['收藏率'].values.tolist()

# 设置画布大小
# 表示图片的大小为宽8inch、高6inch（单位为inch）
plt.figure(figsize=(8,6))
# 绘制柱形图
plt.bar(x,y1)
# 设置标题以及x轴标题，y轴标题
plt.xlabel('词根')
plt.ylabel('访客数')
# 设置数字标签
for a,b in zip(x,y1):
    plt.text(a,b+0.05,'%.0f'% b,ha='center',va='bottom',fontsize=8)
plt.show()

# 设置画布大小
# 表示图片的大小为宽8inch、高6inch（单位为inch）
plt.figure(figsize=(8,6))
# 绘制柱形图
plt.bar(x,y2)
# 设置标题以及x轴标题，y轴标题
plt.xlabel('词根')
plt.ylabel('转化率')
# 设置数字标签
for a,b in zip(x,y2):
    plt.text(a,b,'%.4f'% b,ha='center',va='bottom',fontsize=8)
plt.show()

# 设置画布大小
# 表示图片的大小为宽8inch、高6inch（单位为inch）
plt.figure(figsize=(8,6))
# 绘制柱形图
plt.bar(x,y3)
# 设置标题以及x轴标题，y轴标题
plt.xlabel('词根')
plt.ylabel('加购率')
# 设置数字标签
for a,b in zip(x,y3):
    plt.text(a,b,'%.4f'% b,ha='center',va='bottom',fontsize=8)
plt.show()

# 设置画布大小
# 表示图片的大小为宽8inch、高6inch（单位为inch）
plt.figure(figsize=(8,6))
# 绘制柱形图
plt.bar(x,y4)
# 设置标题以及x轴标题，y轴标题
plt.xlabel('词根')
plt.ylabel('收藏率')
# 设置数字标签
for a,b in zip(x,y4):
    plt.text(a,b,'%.4f'% b,ha='center',va='bottom',fontsize=8)
plt.show()